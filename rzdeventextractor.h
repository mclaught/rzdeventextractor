#ifndef RZDEVENTEXTRACTOR_H
#define RZDEVENTEXTRACTOR_H

#include "rzdeventextractor_global.h"
#include "cmd_code.h"
#include "version.h"

const char version[] = VER_FILEVERSION_STR;

#pragma pack(push, 1)

typedef struct
{
  uint16_t syn;///0x55AA
  uint8_t addr;//адрес конкретного МКУ, если tcp порт общий
  uint16_t pack_len;
}u485_hdr_t;

typedef struct //заголовок данных от МКУ к ПК
{
  uint8_t      device_id;//номера датчиков 0x81 - 0x86, 0x91,0x92
  uint32_t     timestamp;//время в 100мкс интервалах для первого события в пакете
  uint16_t     amount;//кол-во событий event_t в пакете
}data_to_rs485_t;

typedef struct
{
  int16_t         time_shift;//сдвиг между timestamp в заголовке пакета и данным событием
  uint8_t         joint_code;//служебные коды, описание прилагается
  int16_t         value[3]; //измерения по 3-ем осям
}event_t;

typedef struct
{
  uint8_t id;//
  uint8_t parameter;//
  uint8_t code;//
  uint32_t timestamp;//
  int16_t val;//
}data_item_t;

typedef struct
{
  uint32_t timestamp;
  uint8_t type;
  uint16_t amount;
}block_hdr_t;

typedef struct
{
  uint16_t indx;
  int16_t val;
}points_t;

#pragma pack(pop)

class RZDEVENTEXTRACTORSHARED_EXPORT RZDEventExtractor
{

public:
    RZDEventExtractor();
    uint32_t event_to_item (void *buf, event_t *p_ev, data_to_rs485_t *p_485);
    int get_version(char* buf);

private:
};

#endif // RZDEVENTEXTRACTOR_H
