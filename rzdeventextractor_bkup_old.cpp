#include "rzdeventextractor.h"


RZDEventExtractor::RZDEventExtractor()
{
}

#define _24_00_100us	static_cast<uint32_t>(24UL * 3600UL * 10000UL)
#define _24_00_25us     static_cast<uint32_t>(_24_00_100us * 4UL)

uint32_t RZDEventExtractor::event_to_item (void *buf, event_t *p_ev, data_to_rs485_t *p_485)
{uint32_t new_timestamp_100us, new_timestamp_25us;
    uint32_t i_cnt = 0, ev, i, _id;
    data_item_t *p_item = reinterpret_cast<data_item_t *>(buf);

     for( ev = 0; ev < p_485->amount; ev++ )
     {
       if( (new_timestamp_100us = static_cast<uint32_t>(static_cast<int32_t>(p_485->timestamp) + static_cast<int32_t>(p_ev->time_shift))) >= _24_00_100us )
       {new_timestamp_100us -= _24_00_100us;};

       if( (new_timestamp_25us = static_cast<uint32_t>(static_cast<int32_t>(p_485->timestamp) + static_cast<int32_t>(p_ev->time_shift))) >= _24_00_25us )
       {new_timestamp_25us -= _24_00_25us;};

       switch( (_id = (p_485->device_id >> 4)) )
       {
       case 0x07:
       case 0x08://не ДПК
         switch(p_ev->joint_code)
         {
         case CODE_RAW_DATA:
         case CODE_MEDIAN_DATA:
           for( i = 0; i < 3; i++ )
           {
             if( !(p_ev->value[i]) )
             {continue;};

             p_item->id = p_485->device_id;
             p_item->timestamp = new_timestamp_100us;
             p_item->val = p_ev->value[i];
             p_item->code = p_ev->joint_code;
             p_item->parameter = static_cast<uint8_t>(0x21 + i);

             p_item++;
             i_cnt++;
           };
           break;

         case CODE_RAW_DATA_40KHZ:
           for( i = 0; i < 3; i++ )
           {
             //if( !(p_ev->value[i]) )
             //{continue;};

             p_item->id = p_485->device_id;
             p_item->timestamp = (new_timestamp_25us + i);
             p_item->val = p_ev->value[i];
             p_item->code = p_ev->joint_code;
             p_item->parameter = static_cast<uint8_t>(0x21 + i);

             p_item++;
             i_cnt++;
           };
           break;

         case CODE_VERSION_ID:
         case CODE_SET_TIME:
         case CODE_GET_TIME:
         case CODE_BUFFER_RESET:
         case CODE_ACC_KEEPALIVE:
           p_item->id = p_485->device_id;
           p_item->parameter = 0;
           p_item->code = p_ev->joint_code;
           p_item->timestamp = (_id == 0x08 ? new_timestamp_100us : new_timestamp_25us);
           p_item->val = p_ev->value[0];//?

           p_item++;
           i_cnt++;
           break;

         case CODE_FULLBUFFER:
             for( i=0; i<3; i++ ){
                 p_item->id = p_485->device_id;
                 p_item->parameter = static_cast<uint8_t>(i);
                 p_item->code = p_ev->joint_code;
                 p_item->timestamp = new_timestamp_100us;
                 p_item->val = p_ev->value[i];//?

                 p_item++;
                 i_cnt++;
             }
             break;

         default:
           for( i = 0; i < 3; i++ )
           {
             p_item->id = p_485->device_id;
             p_item->timestamp = (_id == 0x08 ? new_timestamp_100us : new_timestamp_25us);
             p_item->val = p_ev->value[i];
             p_item->code = p_ev->joint_code;
             p_item->parameter = static_cast<uint8_t>(0x21 + i);

             p_item++;
             i_cnt++;
           };
           break;
         };
         break;

       case 0x09://ДПК
         p_item->id = p_485->device_id;
         p_item->timestamp = new_timestamp_100us;
         p_item->val = 0;
         p_item->code = 0;
         p_item->parameter = p_ev->joint_code;

         p_item++;
         i_cnt++;
         break;
         
       default:
         p_item->id = 0x9F;
         p_item->parameter = ID_PARAMETER_DEBUG;
         p_item->code = CODE_ID_DEBUG;
         p_item->timestamp = new_timestamp_100us;
         p_item->val = p_ev->value[0];
         
         p_item++;
         i_cnt++;
       };

       p_ev++;
     };

     return i_cnt;
}
