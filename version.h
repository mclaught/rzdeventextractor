#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             01,00,01,00
#define VER_FILEVERSION_STR         "1.0.1"
#define VER_PRODUCTVERSION          1,0,1,0
#define VER_PRODUCTVERSION_STR      "1.0.1\0"
#define VER_FILEDESCRIPTION_STR     "���� �����. EventExtractor"
#define VER_INTERNALNAME_STR        "���� �����. EventExtractor"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (C) 2018, ���� �� \"������\""
#define VER_ORIGINALFILENAME_STR    "RZDEventExtractor.dll"
#define VER_PRODUCTNAME_STR         "DLL ���������� ������� ��� ���� �����"

#endif // VERSION_H
