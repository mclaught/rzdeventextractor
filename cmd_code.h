#ifndef CMD_CODE_H
#define CMD_CODE_H

#define CODE_VERSION_ID             0x00

#define CODE_SET_TIME               0x01
#define CODE_GET_TIME               0x02

#define CODE_BUFFER_RESET           0x03
#define CODE_TEST_TIME              0x04
#define CODE_ACC_READFIFO           0x05
#define CODE_ACC_OFFSET             0x06
#define CODE_ACC_DEVIATION          0x07
#define CODE_ACC_STEP_LEVEL         0x08
#define CODE_FULLBUFFER             0x09
#define CODE_ACC_LEVEL_UP           0x0A
#define CODE_ACC_LEVEL_DOWN         0x0B
#define CODE_ACC_MAX_DEVIATION      0x0C
#define CODE_ACC_LEVEL_RESET        0x0D
#define CODE_ACC_VALUE_TRSHLD       0x0E

#define CODE_RAW_DATA               0x10
#define CODE_MEDIAN_DATA            0x12
#define CODE_RAW_DATA_40KHZ         0x14

#define CODE_LOW_LIMIT              0x20
#define CODE_HIGH_LIMIT             0x21
#define CODE_AVERAGE                0x22

#define CODE_ACC_FIFO_DEBUG         0x40
#define CODE_MKU_TIME_SYNC_DEBUG    0x42
#define CODE_MKU_TIME_MSG_DEBUG     0x43
#define CODE_MKU_FW_ID              0x44
#define CODE_MKU_REQ_DEBUG          0x45
#define CODE_MKU_RESP_DEBUG         0x46
#define CODE_ACC_REQ_DEBUG          0x47
#define CODE_ACC_REQ_CRR_ERR        0x48
#define CODE_ACC_CLK_FAILURE        0x49
#define CODE_ACC_INFO               0x4A
#define CODE_ACC_KEEPALIVE          0xFF

#define CODE_ID_DEBUG               0x75

#define ID_PARAMETER_TIME           0x00
#define ID_PARAMETER_ACC_ALL        0x20
#define ID_PARAMETER_ACC_X          0x21
#define ID_PARAMETER_ACC_Y          0x22
#define ID_PARAMETER_ACC_Z          0x23

#define ID_PARAMETER_DEBUG          0x75

#endif
